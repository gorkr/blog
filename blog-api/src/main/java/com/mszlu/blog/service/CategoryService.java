package com.mszlu.blog.service;

import com.mszlu.blog.vo.CategoryVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author gorkr
 * @title: CategoryService
 * @projectName blog-api
 * @description: TODO
 * @date 2022/3/819:16
 */
public interface CategoryService {
    CategoryVo findCategoryById(Long id);

    List<CategoryVo> findAll();
}
