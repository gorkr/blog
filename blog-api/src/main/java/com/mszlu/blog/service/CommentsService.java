package com.mszlu.blog.service;

import com.mszlu.blog.vo.CommentVo;
import com.mszlu.blog.vo.params.CommentParams;

import java.util.List;

/**
 * @author gorkr
 * @date 2022/03/09 11:43
 **/
public interface CommentsService {

    List<CommentVo> findCommentsByArticleId(Long articleId);

    void addComment(CommentParams commentParams);
}
    