package com.mszlu.blog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mszlu.blog.dao.mapper.CommentsMapper;
import com.mszlu.blog.dao.mapper.SysUserMapper;
import com.mszlu.blog.dao.pojo.Comment;
import com.mszlu.blog.dao.pojo.SysUser;
import com.mszlu.blog.service.CommentsService;
import com.mszlu.blog.utils.UserThreadLocal;
import com.mszlu.blog.vo.CommentVo;
import com.mszlu.blog.vo.UserVo;
import com.mszlu.blog.vo.params.CommentParams;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gorkr
 * @date 2022/03/09 11:45
 **/
@Service
@Transactional
public class CommentsServiceImpl implements CommentsService {

    @Autowired
    private CommentsMapper commentsMapper;

    @Autowired
    private SysUserMapper sysUserMapper;



    /**
     *
     * @return
     *
     * 1. 根据文章id 查询 评价列表 从 comment 表中查询
     * 2. 判断作者的id 查询作者信息
     * 3. 判断 如果 level =1 要去查询它有没有子评论
     * 4. 如果有  根据评论id 进行查询 （ parent_id)
     *
     */
    @Override
    public List<CommentVo> findCommentsByArticleId(Long articleId) {
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getArticleId, articleId);
        List<Comment> comments = commentsMapper.selectList(queryWrapper);
        List<CommentVo> commentVoList = copyList(comments);

        return commentVoList;
    }

    @Override
    public void addComment(CommentParams commentParams) {
        System.out.println("add???");
        SysUser user = UserThreadLocal.get();
        Comment comment = new Comment();
        comment.setArticleId(commentParams.getArticleId());
        comment.setAuthorId(user.getId());
        comment.setContent(commentParams.getContent());
        comment.setCreateDate(System.currentTimeMillis());
        Long parent = commentParams.getParent();
        if (parent == null || parent == 0) {
            comment.setLevel(1);
        }else{
            comment.setLevel(2);
        }
        comment.setParentId(parent == null ? 0 : parent);
        Long toUserId = commentParams.getToUserId();
        comment.setToUid(toUserId == null ? 0 : toUserId);
        commentsMapper.insert(comment); // 这里怎么判断添加有没有成功呢？
    }

    private List<CommentVo> copyList(List<Comment> comments){
        ArrayList<CommentVo> commentVos = new ArrayList<>();
        for (Comment comment: comments
             ) {

            commentVos.add(copy(comment));
        }
        return commentVos;

    }

    private CommentVo copy(Comment comment) {
        CommentVo commentVo = new CommentVo();
        BeanUtils.copyProperties(comment,commentVo);

        // 转换时间格式
        commentVo.setCreateDate(new DateTime(comment.getCreateDate()).toString("yyyy-MM-dd HH:mm"));

        // 评论作者信息
        Long authorId = comment.getAuthorId();
        SysUser sysUser = sysUserMapper.selectById(authorId);
        UserVo userVo = new UserVo();  // 这部分的确不该在这里写
        BeanUtils.copyProperties(sysUser,userVo);
        commentVo.setAuthor(userVo);

        // 是否有子评论
        List<CommentVo> commentVoList = findCommentsByParentId(comment.getId());
        commentVo.setChildrens(commentVoList);
        if(comment.getLevel()>1){
            Long toUid = comment.getToUid();
            UserVo toUserVo = new UserVo();
            SysUser sysUser1 = sysUserMapper.selectById(toUid);
            BeanUtils.copyProperties(sysUser1,toUserVo);
            commentVo.setToUser(toUserVo);
        }

        return commentVo;
    }

    private List<CommentVo> findCommentsByParentId(Long id) {
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getParentId,id);
        queryWrapper.eq(Comment::getLevel,2);
        List<Comment> comments = this.commentsMapper.selectList(queryWrapper);
        return copyList(comments);

    }


}
    