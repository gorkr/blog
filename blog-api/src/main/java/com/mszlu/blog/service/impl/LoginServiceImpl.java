package com.mszlu.blog.service.impl;

import com.alibaba.fastjson.JSON;
import com.mszlu.blog.dao.pojo.SysUser;
import com.mszlu.blog.service.LoginService;
import com.mszlu.blog.service.SysUserService;
import com.mszlu.blog.utils.JWTUtils;
import com.mszlu.blog.vo.ErrorCode;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.LoginParams;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.concurrent.TimeUnit;


@Service
@Transactional  // 增加事务
public class LoginServiceImpl implements LoginService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private static final String slat = "mszlu!@#";

    @Override
    public Result login(LoginParams loginParams) {
        /**
         * 检查参数是否合法
         * 根据用户和密码去user表查询
         * 如果不存在 登陆失败
         * 如果存在 使用jwt生成token 返回其那段
         * token放入redis中， redis中  token:user 设置过期时间
         * （登录认证的时候， 先认证token是否合法，去redis验证是否存在)
         */
        String account = loginParams.getAccount();
        String password = loginParams.getPassword();
        if(StringUtils.isBlank(account) || StringUtils.isBlank(password)){
            // Java 枚举类型
            return Result.fail(ErrorCode.PARAMS_ERROR.getCode(), ErrorCode.PARAMS_ERROR.getMsg());
        }

        // 解密
        password = DigestUtils.md5Hex(password + slat);  // 也可以从数据库中那个获取加密盐

        SysUser user =  sysUserService.findUser(account, password);
        if (user== null){
            return  Result.fail(ErrorCode.ACCOUNT_PWD_NOT_EXIST.getCode(), ErrorCode.ACCOUNT_PWD_NOT_EXIST.getMsg());
        }

        // jwt生成token
        String token = JWTUtils.createToken(user.getId());

        // token放入redis中， redis中  token:user 设置过期时间
        redisTemplate.opsForValue().set("TOKEN_"+token, JSON.toJSONString(user),1, TimeUnit.DAYS);
        System.out.println("token"+token);
        return Result.success(token);
    }

    @Override
    public SysUser checkToken(String token) {

        if(StringUtils.isBlank(token)){
            return null;
        }
        Map<String, Object> stringObjectMap = JWTUtils.checkToken(token);  // 解析token
        if(stringObjectMap == null){
            return null;
        }
        String userJson = redisTemplate.opsForValue().get("TOKEN_" + token);
        if(StringUtils.isBlank(userJson)){
            return null;
        }
        return JSON.parseObject(userJson, SysUser.class);

    }

    @Override
    public Result logout(String token) {
        // 将redis中缓存的token删除
        redisTemplate.delete("TOKEN_"+token);
        return Result.success(null);
    }

    @Override
    public Result register(LoginParams loginParams) {
        /**
         * 1. 判断参数是否正确（为空，或者位数）
         * 2. 是否已经注册
         * 3. 存入数据库据 并返回token
         * 4. 将token存入redis
         * 5. 使用事务保证数据库更新正确  哪里是使用了事务  //@Transactional 注解增加事务
         */

        // 判断获取的参数是否正确
        String account = loginParams.getAccount();
        String password = loginParams.getPassword();
        String nickname = loginParams.getNickname();

        if(StringUtils.isBlank(account)
        || StringUtils.isBlank(password)
        || StringUtils.isBlank(nickname)){
            return Result.fail(ErrorCode.PARAMS_ERROR.getCode(), ErrorCode.PARAMS_ERROR.getMsg());
        }

        // 判断数据库中是否已存在该用户
        SysUser sysUser = sysUserService.findUserByAccount(account);
        if(sysUser!=null){
            return Result.fail(ErrorCode.ACCOUNT_EXIST.getCode(),"用户已存在");
        }


        // 返回空指针没有类型嘛？？  为什么不能直接用
        sysUser = new SysUser();

        sysUser.setAccount(account);
        // 密码加密存储
        sysUser.setPassword(DigestUtils.md5Hex(password+slat));
        sysUser.setNickname(nickname);
        sysUser.setCreateDate(System.currentTimeMillis());
        sysUser.setLastLogin(System.currentTimeMillis());
        sysUser.setAdmin(1); //1 为true
        sysUser.setDeleted(0); // 0 为false

        // 存入数据库  使用事务？？
        sysUserService.save(sysUser);
        // 生成token ， 并存入redis

        String token = JWTUtils.createToken(sysUser.getId());  // 这个id从哪来， 插入以后就有id了嘛

        redisTemplate.opsForValue().set("TOKEN_"+token, JSON.toJSONString(sysUser), 1, TimeUnit.DAYS);
        return Result.success(token);
    }
}
