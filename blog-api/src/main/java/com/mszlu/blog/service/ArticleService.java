package com.mszlu.blog.service;

import com.mszlu.blog.dao.pojo.ArticleBody;
import com.mszlu.blog.vo.ArticleBodyVo;
import com.mszlu.blog.vo.ArticleVo;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.ArticleParam;
import com.mszlu.blog.vo.params.PageParams;

public interface ArticleService  {
    /**
     * 分页查询文章列表
     * @param pageParams
     * @return
     */
    Result listArticle(PageParams pageParams);

    Result hotArticle(int limit);

    /**
     * 首页最新文章
     * @param limit
     * @return
     */
    Result newArticle(int limit);

    /**
     * 文章归档
     * @return
     */
    Result listArchives();

    ArticleVo getArticleById(long id);

    ArticleBodyVo findArticleBodyById(long id);

    ArticleVo publish(ArticleParam articleParam);
}
