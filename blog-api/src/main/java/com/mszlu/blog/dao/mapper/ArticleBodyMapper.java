package com.mszlu.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mszlu.blog.dao.pojo.ArticleBody;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author gorkr
 * @title: ArticleBodyMapper
 * @projectName blog-api
 * @description: TODO
 * @date 2022/3/819:03
 */
@Mapper
public interface ArticleBodyMapper extends BaseMapper<ArticleBody> {
}
