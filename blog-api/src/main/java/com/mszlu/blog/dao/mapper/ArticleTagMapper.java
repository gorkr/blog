package com.mszlu.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mszlu.blog.dao.pojo.ArticleTag;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author gorkr
 * @date 2022/03/09 21:18
 **/
@Mapper
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
}
    