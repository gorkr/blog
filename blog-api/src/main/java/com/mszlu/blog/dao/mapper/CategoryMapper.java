package com.mszlu.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mszlu.blog.dao.pojo.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author gorkr
 * @date 2022/03/08 19:18
 **/
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
    