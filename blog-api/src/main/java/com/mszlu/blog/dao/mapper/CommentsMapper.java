package com.mszlu.blog.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mszlu.blog.dao.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author gorkr
 * @title: CommentsMapper
 * @projectName blog-api
 * @description: TODO
 * @date 2022/3/911:49
 */
@Mapper
public interface CommentsMapper extends BaseMapper<Comment> {

}
