package com.mszlu.blog.controller;

import com.mszlu.blog.service.TagService;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.TagVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("tags")
public class TagsController {

    @Autowired
    private TagService tagService;


    @GetMapping("/hot")
    public Result listHotTags(){ //  这里因该传参的吧
        int limit = 6;  // 热门标签数量
        List<TagVo> tagVoList = tagService.hot(limit);  // 这比玩意是空的
        return Result.success(tagVoList);

    }

    @GetMapping
    public Result listTags(){
        List<TagVo> tagVos = tagService.finAll();
        return Result.success(tagVos);
    }
}
