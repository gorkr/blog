package com.mszlu.blog.controller;

import com.mszlu.blog.service.CommentsService;
import com.mszlu.blog.vo.CommentVo;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.CommentParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author gorkr
 * @date 2022/03/09 11:31
 **/
@RestController
@RequestMapping("comments")
public class CommentController {

    @Autowired
    private CommentsService commentsService;

    /**
     * 获取文章评论
     * @param id 文章id
     * @return
     */
    @GetMapping("/article/{id}")
    public Result getComments(@PathVariable("id") Long id){
        System.out.println("comments");

        List<CommentVo> commentsByArticleId = commentsService.findCommentsByArticleId(id);
        System.out.println(commentsByArticleId);
        return Result.success(commentsByArticleId);

    }


    @PostMapping("create/change")
    public Result addComment(@RequestBody CommentParams commentParams){

        commentsService.addComment(commentParams); //
        return Result.success(null);
    }
}
    