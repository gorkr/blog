package com.mszlu.blog.controller;

import com.mszlu.blog.dao.pojo.SysUser;
import com.mszlu.blog.utils.UserThreadLocal;
import com.mszlu.blog.vo.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author gorkr
 * @title: TestController
 * @projectName blog-api
 * @description: TODO
 * @date 2021/11/1012:36
 */

@RestController
@RequestMapping("test")
public class TestController {

    @RequestMapping()
    public Result test(){
        SysUser sysUser = UserThreadLocal.get();
        System.out.println(sysUser);
        System.out.println("test");
        return Result.success(null);
    }
}
