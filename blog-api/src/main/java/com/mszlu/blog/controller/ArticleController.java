package com.mszlu.blog.controller;


import com.mszlu.blog.service.ArticleService;
import com.mszlu.blog.service.TagService;
import com.mszlu.blog.vo.ArticleVo;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.ArticleParam;
import com.mszlu.blog.vo.params.PageParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin  给这个controller设置跨域
@RestController  //json数据进行交互
@RequestMapping("articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;




    /**
     * 首页 文章列表
     *
     * @param pageParams
     * @return
     */
    @PostMapping
    public Result listArticle(@RequestBody PageParams pageParams) {
        // 业务逻辑从Service处理

        return articleService.listArticle(pageParams);
    }


    @PostMapping("hot")
    public Result hotArticle() {
        int limit = 5;
        return articleService.hotArticle(limit);
    }

    @PostMapping("new")
    public Result newArticle(){
        int limit = 5;
        return articleService.newArticle(limit);
    }

    @PostMapping("listArchives")
    public Result listArchives(){
        return articleService.listArchives();
    }

    @PostMapping("view/{id}")
    public Result getArticleDetial(@PathVariable("id") long id){
        final ArticleVo articleById = articleService.getArticleById(id);
        return Result.success(articleById);
    }

    @PostMapping("publish")
    public Result publish(@RequestBody ArticleParam articleParam){
        ArticleVo articleVo = articleService.publish(articleParam);
        return Result.success(articleVo); // 插入成功怎么整
    }
}
