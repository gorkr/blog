package com.mszlu.blog.controller;


import com.mszlu.blog.service.LoginService;
import com.mszlu.blog.service.SysUserService;
import com.mszlu.blog.vo.Result;
import com.mszlu.blog.vo.params.LoginParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("login")
public class LoginController {

//    @Autowired
//    private SysUserService sysUserService
//    登录应该是一个业务service

    @Autowired
    private LoginService loginService;  //  写一个业务去做相关工作

    @PostMapping
    public Result login(@RequestBody LoginParams loginParams){
        // 登录 验证用户 访问用户表， 但是
        return loginService.login(loginParams);
    }
}
