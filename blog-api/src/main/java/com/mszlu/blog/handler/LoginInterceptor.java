package com.mszlu.blog.handler;

import com.alibaba.fastjson.JSON;
import com.mszlu.blog.dao.pojo.SysUser;
import com.mszlu.blog.service.LoginService;
import com.mszlu.blog.utils.UserThreadLocal;
import com.mszlu.blog.vo.ErrorCode;
import com.mszlu.blog.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author gorkr
 * @title: LoginInterceptor
 * @projectName blog-api
 * @description: TODO
 * @date 2021/11/819:58
 */



// lombok里带有Slf4j注释，用于日志
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private LoginService loginService;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 在执行controller(handler)方法之前执行
        /**
         * 1. 判断 请求的接口路径 是否为 HandlerMethod (controller方法)
         * 2. 判断 token是否未空， 如果为空 未登录
         * 3. 如果 toekn 不为空， 验证登录
         * 4. 如果认证成功 放行
         */

        System.out.println("执行拦截器");
        if(!(handler instanceof HandlerMethod)){
            System.out.println("static");
            // handler 可能是 RequestResourceHandler 访问静态资源， 默认去classpath下的static 目录去查找
            return true;
        }
        System.out.println(request);
        String token = request.getHeader("Authorization");
        System.out.println(request);

        log.info("=================request start===========================");
        String requestURI = request.getRequestURI();
        log.info("request uri:{}",requestURI);
        log.info("request method:{}",request.getMethod());
        log.info("token:{}", token);
        log.info("=================request end===========================");

        if(StringUtils.isBlank(token)){
            System.out.println("1");
            // 不放行信息传到前端
            Result result = Result.fail(ErrorCode.NO_LOGIN.getCode(), "未登录");
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSON.toJSONBytes(result));
            return false;
        }
        SysUser sysUser = loginService.checkToken(token);

        if(sysUser==null){
            System.out.println("2");
            final Result result = Result.fail(ErrorCode.NO_LOGIN.getCode(), "未登录");
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSON.toJSONString(result));
            return false;
        }

        // 希望在controller中 直接获取用户信息  怎么获取
        // 登录验证成功，放行
        // threadLocal 保存用户信息

        UserThreadLocal.put(sysUser);
        System.out.println("拦截器1\n"+sysUser);
        System.out.println("拦截器\n"+UserThreadLocal.get());
        System.out.println("拦截器放行");
        return true;
    }


}
