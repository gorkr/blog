package com.mszlu.blog.vo.params;

import lombok.Data;

/**
 * @author gorkr
 * @date 2022/03/09 14:55
 **/
@Data
public class CommentParams {
    private Long articleId;
    // 评论内容
    private String content;

    // 父评论id
    private Long parent;

    // 被评价用户id
    private Long toUserId;

}
    