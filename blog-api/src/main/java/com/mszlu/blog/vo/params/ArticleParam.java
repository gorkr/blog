package com.mszlu.blog.vo.params;

import com.mszlu.blog.dao.pojo.Category;
import com.mszlu.blog.vo.CategoryVo;
import com.mszlu.blog.vo.TagVo;
import lombok.Data;

import java.util.List;

/**
 * @author gorkr
 * @date 2022/03/09 19:07
 **/
@Data
public class ArticleParam {

    private Long id;

    private String title;

    private ArticleBodyParm body;

    private Category category;

    private String summary;

    private List<TagVo> tags;
}
    