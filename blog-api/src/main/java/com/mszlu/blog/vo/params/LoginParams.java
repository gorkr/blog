package com.mszlu.blog.vo.params;


import lombok.Data;

/**
 * @author gorkr
 */
@Data
public class LoginParams {

    private String account;
    private String password;
    private String nickname;
}
