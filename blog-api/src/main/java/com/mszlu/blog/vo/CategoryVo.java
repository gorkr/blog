package com.mszlu.blog.vo;

import lombok.Data;

/**
 * @author gorkr
 * @date 2022/03/08 18:40
 **/
@Data
public class CategoryVo {
    private String avatar;

    private String categoryName;

    private String description;
}
    