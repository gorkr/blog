package com.mszlu.blog.vo;

import lombok.Data;

/**
 * @author gorkr
 * @date 2022/03/08 18:39
 **/
@Data
public class ArticleBodyVo {

    private String content;
}
    