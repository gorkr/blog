package com.mszlu.blog.vo.params;

import lombok.Data;

/**
 * @author gorkr
 * @date 2022/03/09 19:10
 **/

@Data
public class ArticleBodyParm {

    private String content;

    private String contentHtml;
}
    